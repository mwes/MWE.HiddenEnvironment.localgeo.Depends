Linked project: https://gitlab.com/mwes/MWE.HiddenEnvironment.localgeo.Import

As noted in the original [issue](https://stackoverflow.com/questions/55799141/how-to-easily-access-hidden-environment-in-package2-from-package1), the goal is to have easy access to the hidden environments and their objects in Otherpackage when writing package1.

>This MWE should return `data` with a lat and lon for the three given city/state pairs.
It succeeds. This can be verified by `devtools::check()`.
See linked MWE (MWE.HiddenEnvironment.localgeo.Import) for identical setup that fails.

Current solutions:
*  Use `Depends` rather than `Import` in [package1](https://gitlab.com/mwes/MWE.HiddenEnvironment.localgeo.Depends)
*  Recreate Otherpackage's hidden environment in package1
*  Ask Otherpackage to change their package

Can anyone successfully use `Import` in package1 and avoid the current solutions?

Tags: `R`, `hidden-files`
